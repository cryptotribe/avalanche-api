'use strict';

module.exports = function(app) {

  const AVMController = require('../controllers/AVMController');
  const EVMController = require('../controllers/EVMController');
  const ETHController = require('../controllers/ETHController');

  app.route('/status')
    .get(AVMController.status);

  app.route('/create_keys')
    .post(AVMController.create_keys);

  app.route('/send_avax')
    .post(AVMController.send_xchain_avax);

  app.route('/export_x2c_avax')
    .post(AVMController.export_x2c_avax);

  app.route('/import_c2x_avax')
    .post(AVMController.import_c2x_avax);

  app.route('/export_evm_xchain_avax')
    .post(EVMController.export_evm_xchain_avax);

  app.route('/import_evm_xchain_avax')
    .post(EVMController.import_evm_xchain_avax);

  app.route('/get_cchain_address')
    .post(EVMController.get_cchain_address);

  app.route('/get_cb58_private_key')
    .post(EVMController.get_cb58_private_key);

  app.route('/get_hex_address')
    .post(ETHController.get_hex_address);

  app.route('/get_hex_private_key')
    .post(ETHController.get_hex_private_key);


};

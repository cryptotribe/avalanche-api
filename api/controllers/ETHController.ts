import { Avalanche, BinTools, Buffer as AvalancheBuffer } from "avalanche"
import { privateToAddress } from "ethereumjs-util"


exports.get_hex_address = async function(req, res) {
  const privateKey = req.body.privateKey;
  const bintools: BinTools = BinTools.getInstance()
  const privKeyBuffer: AvalancheBuffer = bintools.cb58Decode(
    privateKey.split("-")[1]
  )
  const privKeyHex: string = privKeyBuffer.toString("hex")
  const correctBuffer: Buffer = Buffer.from(privKeyHex, "hex")
  const hexAddress: string = privateToAddress(correctBuffer).toString("hex")
  res.json("0x"+hexAddress)
}

exports.get_hex_private_key = async function(req, res) {
  const privateKey = req.body.privateKey;
  const bintools: BinTools = BinTools.getInstance()
  const privKeyBuffer: AvalancheBuffer = bintools.cb58Decode(
    privateKey.split("-")[1]
  )
  const privKeyHex: string = privKeyBuffer.toString("hex")
  res.json(privKeyHex)
}

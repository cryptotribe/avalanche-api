import { Avalanche, BN, Buffer, BinTools } from "avalanche"
import {
  AVMAPI,
  KeyChain as AVMKeyChain
} from "avalanche/dist/apis/avm"
import {
  EVMAPI,
  KeyChain as EVMKeyChain,
  UnsignedTx,
  Tx,
  UTXOSet
} from "avalanche/dist/apis/evm"
import {
  PrivateKeyPrefix,
  DefaultLocalGenesisPrivateKey,
  Defaults
} from "avalanche/dist/utils"
import Mnemonic from "avalanche/dist/utils/mnemonic"
import HDNode from "avalanche/dist/utils/hdnode"


exports.get_cchain_address = async function(req, res) {
  const networkID = req.body.networkID;
  const endpoint = req.body.endpoint;
  const privateKey = req.body.privateKey;
  const avalanche: Avalanche = new Avalanche(endpoint, 443, "https", networkID)
  const cchain = avalanche.CChain()
  const keychainC = cchain.keyChain()
  const mnemonic: Mnemonic = Mnemonic.getInstance()
  const mnemonicToSeed: Buffer = await mnemonic.mnemonicToSeed(privateKey, "")
  const fromSeed = new HDNode(mnemonicToSeed)
  const child = fromSeed.derive("m/44'/60'/0'/0/0")
  keychainC.importKey(child.privateKeyCB58)
  const addressC = keychainC.getAddressStrings()
  res.json(addressC)
}

exports.get_cb58_private_key = async function(req, res) {
  const networkID = req.body.networkID;
  const endpoint = req.body.endpoint;
  const privateKey = req.body.privateKey;
  const avalanche: Avalanche = new Avalanche(endpoint, 443, "https", networkID)
  const cchain = avalanche.CChain()
  const keychainC = cchain.keyChain()
  const mnemonic: Mnemonic = Mnemonic.getInstance()
  const mnemonicToSeed: Buffer = await mnemonic.mnemonicToSeed(privateKey, "")
  const fromSeed = new HDNode(mnemonicToSeed)
  const child = fromSeed.derive("m/44'/60'/0'/0/0")
  res.json(child.privateKeyCB58)
}

// EVM utility method buildExportTx to export AVAX to the C-Chain from the X-Chain
exports.export_evm_xchain_avax = async function(req, res) {
  const networkID = req.body.networkID;
  const endpoint = req.body.endpoint;
  const privateKey = req.body.privateKey;
  const amount = req.body.amount;
  const cHexAddress = req.body.toAddress; // HEX ETH format address
  const avalanche: Avalanche = new Avalanche(endpoint, 443, "https", networkID)
  const xchain: AVMAPI = avalanche.XChain()
  const cchain: EVMAPI = avalanche.CChain()
  const xKeychain: AVMKeyChain = xchain.keyChain()
  const privKey: string = `${PrivateKeyPrefix}${DefaultLocalGenesisPrivateKey}`
  const cKeychain: EVMKeyChain = cchain.keyChain()
  xKeychain.importKey(privKey)
  cKeychain.importKey(privKey)
  const xAddressStrings: string[] = xchain.keyChain().getAddressStrings()
  const cAddressStrings: string[] = cchain.keyChain().getAddressStrings()
  const xChainBlockchainIdStr: string = Defaults.network[networkID].X.blockchainID
  const avaxAssetID: string = Defaults.network[networkID].X.avaxAssetID
  const Web3 = require("web3")
  const path: string = "/ext/bc/C/rpc"
  const web3 = new Web3(`https://${endpoint}${path}`)
  const threshold: number = 1

  const txcount = await web3.eth.getTransactionCount(cHexAddress)
  const nonce: number = txcount
  const locktime: BN = new BN(0)
  const avaxAmount: BN = new BN(amount)

  const unsignedTx: UnsignedTx = await cchain.buildExportTx(
    avaxAmount,
    avaxAssetID,
    xChainBlockchainIdStr,
    cHexAddress,
    cAddressStrings[0],
    xAddressStrings,
    nonce,
    locktime,
    threshold
  )

  const tx: Tx = unsignedTx.sign(cKeychain)
  const txid: string = await cchain.issueTx(tx)
  res.json(txid)
};


// EVM utility method buildImportTx to import AVAX to the X-Chain from the C-Chain
exports.import_evm_xchain_avax = async function(req, res) {
  const networkID = req.body.networkID;
  const endpoint = req.body.endpoint;
  const privateKey = req.body.privateKey;
  const cHexAddress = req.body.toAddress; // HEX ETH format address
  const avalanche: Avalanche = new Avalanche(endpoint, 443, "https", networkID)
  const xchain: AVMAPI = avalanche.XChain()
  const cchain: EVMAPI = avalanche.CChain()
  const xKeychain: AVMKeyChain = xchain.keyChain()
  const cKeychain: EVMKeyChain = cchain.keyChain()
  xKeychain.importKey(privateKey)
  cKeychain.importKey(privateKey)
  const cAddressStrings: string[] = cchain.keyChain().getAddressStrings()
  const xChainBlockchainId: string = Defaults.network[networkID].X.blockchainID

  const evmUTXOResponse: any = await cchain.getUTXOs(
    cAddressStrings,
    xChainBlockchainId
  )
  const utxoSet: UTXOSet = evmUTXOResponse.utxos

  const unsignedTx: UnsignedTx = await cchain.buildImportTx(
    utxoSet,
    cHexAddress,
    cAddressStrings,
    xChainBlockchainId,
    cAddressStrings
  )

  const tx: Tx = unsignedTx.sign(cKeychain)
  const txid: string = await cchain.issueTx(tx)
  res.json(txid)
};

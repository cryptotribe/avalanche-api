import { Avalanche, BinTools, BN, Buffer } from "avalanche"
import {
  AVMAPI,
  KeyChain as AVMKeyChain,
  UTXOSet,
  UnsignedTx,
  Tx
} from "avalanche/dist/apis/avm"
import { KeyChain as EVMKeyChain, EVMAPI } from "avalanche/dist/apis/evm"
import {
  PrivateKeyPrefix,
  DefaultLocalGenesisPrivateKey,
  Defaults,
  UnixNow
} from "avalanche/dist/utils"

exports.status = function(req, res) {
  res.json({"status": "OK"});
};

exports.create_keys = function(req, res) {
  let networkID = req.body.networkID;
  let endpoint = req.body.endpoint;
  let avax = new Avalanche(endpoint, 443, "https", networkID);
  let xchain = avax.XChain();
  let keychain = xchain.keyChain();
  let keypair = keychain.makeKey();
  let response = {
    "address": keypair.getAddressString(),
    "publicKey": keypair.getPublicKeyString(),
    "privateKey": keypair.getPrivateKeyString()
  }
  res.json(response);
};

exports.send_xchain_avax = async function(req, res) {
  const networkID = req.body.networkID;
  const endpoint = req.body.endpoint;
  const privateKey = req.body.privateKey;
  const amount = req.body.amount;
  const toAddress = req.body.toAddress;
  const memo = req.body.memo;
  const assetID = Defaults.network[networkID].X.avaxAssetID
  const avax = new Avalanche(endpoint, 443, "https", networkID);
  const xchain = avax.XChain();
  const keychain = xchain.keyChain();
  keychain.importKey(privateKey);
  const addressStrings = keychain.getAddressStrings();
  const UTXOSet = await xchain.getUTXOs(addressStrings);
  const sendAmount = new BN(amount);
  const unsignedTx = await xchain.buildBaseTx(UTXOSet.utxos, sendAmount, assetID, [toAddress], addressStrings, addressStrings, Buffer.from(memo));
  const signedTx = unsignedTx.sign(keychain);
  const txid = await xchain.issueTx(signedTx);
  res.json(txid);
};

// AVM utility method buildExportTx to export AVAX to the C-Chain from the X-Chain
exports.export_x2c_avax = async function(req, res) {
  const networkID = req.body.networkID;
  const endpoint = req.body.endpoint;
  const privateKey = req.body.privateKey;
  const amount = req.body.amount;
  const memo = req.body.memo;
  const avaxAssetID: string = Defaults.network[networkID].X.avaxAssetID;
  const cChainBlockchainID: string = Defaults.network[networkID].C.blockchainID;
  const avax: Avalanche = new Avalanche(endpoint, 443, "https", networkID);
  const xchain: AVMAPI = avax.XChain();
  const cchain: EVMAPI = avax.CChain();
  const xKeychain: AVMKeyChain = xchain.keyChain();
  const cKeychain: EVMKeyChain = cchain.keyChain();
  xKeychain.importKey(privateKey);
  cKeychain.importKey(privateKey);
  const xAddressStrings: string[] = xchain.keyChain().getAddressStrings();
  const cAddressStrings: string[] = cchain.keyChain().getAddressStrings();
  const locktime: BN = new BN(0);
  const asOf: BN = UnixNow();
  const avmUTXOResponse: any = await xchain.getUTXOs(xAddressStrings);
  const utxoSet: UTXOSet = avmUTXOResponse.utxos;
  const getBalanceResponse: any = await xchain.getBalance(xAddressStrings[0], avaxAssetID);
  const sendAmount: BN = new BN(amount);
  const unsignedTx: UnsignedTx = await xchain.buildExportTx(
    utxoSet,
    sendAmount,
    cChainBlockchainID,
    cAddressStrings,
    xAddressStrings,
    xAddressStrings,
    Buffer.from(memo),
    asOf,
    locktime
  );
  const tx: Tx = unsignedTx.sign(xKeychain);
  const txid: string = await xchain.issueTx(tx);
  res.json(txid);
};

// AVM utility method buildImportTx to import AVAX to the X-Chain from the C-Chain
exports.import_c2x_avax = async function(req, res) {
  const networkID = req.body.networkID;
  const endpoint = req.body.endpoint;
  const privateKey = req.body.privateKey;
  const memo = req.body.memo;
  const avalanche: Avalanche = new Avalanche(endpoint, 443, "https", networkID)
  const xchain: AVMAPI = avalanche.XChain()
  const bintools: BinTools = BinTools.getInstance()
  const xKeychain: AVMKeyChain = xchain.keyChain()
  xKeychain.importKey(privateKey)
  const xAddressStrings: string[] = xchain.keyChain().getAddressStrings()
  const cChainBlockchainID: string = Defaults.network[networkID].C.blockchainID
  const threshold: number = 1
  const locktime: BN = new BN(0)
  const asOf: BN = UnixNow()
  const avmUTXOResponse: any = await xchain.getUTXOs(
    xAddressStrings,
    cChainBlockchainID
  )
  const utxoSet: UTXOSet = avmUTXOResponse.utxos

  const unsignedTx: UnsignedTx = await xchain.buildImportTx(
    utxoSet,
    xAddressStrings,
    cChainBlockchainID,
    xAddressStrings,
    xAddressStrings,
    xAddressStrings,
    Buffer.from(memo),
    asOf,
    locktime,
    threshold
  )
  const tx: Tx = unsignedTx.sign(xKeychain)
  const txid: string = await xchain.issueTx(tx)
  res.json(txid);
};

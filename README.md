NodeJS backend service wrapping AvalancheJS API and exposing as REST API

Build: yarn install

Run: npm start

Fuji Testnet endpoint: api.avax-test.network

Avalanche endpoint: api.avax.network


1) Create new account keys:
```
POST http://localhost:3000/create_keys
Body: {
    "networkID": 5,
    "endpoint": "api.avax-test.network"
}
Response:
{
    "address": "X-fuji1fxugsy4jfqglf4cjtux90auczpe7cy5wwker68",
    "publicKey": "8NvixosNXDEr2nv7579eYerEa6SKiKNg6h6dEscygsddKdc538",
    "privateKey": "PrivateKey-2qarB8gPPWg62Cv6HTH6au4LDLMzgRNRfA2SfNivT3sm7ZZdPw"
}
```

You can fund test accounts using faucet:
https://faucet.avax-test.network/


2) Create and sign transaction - transfer AVAX to another account on X Chain:
Note amount in NAVAX units: 1 AVAX == 1000000000 NAVAX

```
POST http://localhost:3000/send_avax
Body: {
    "networkID": 5,
    "endpoint": "api.avax-test.network",
    "privateKey": "PrivateKey-2qarB8gPPWg62Cv6HTH6au4LDLMzgRNRfA2SfNivT3sm7ZZdPw",
    "amount": 1000000000,
    "toAddress": "X-fuji1fxugsy4jfqglf4cjtux90auczpe7cy5wwker68",
    "memo": "X chain transfer"
}
Response:

"9ypCC56tT1ZN3FbjjP6Vq3YJRLN1sitaRjnsHmyhHRhK6PHsb"

```
Tx details: https://explorer.avax-test.network/tx/9ypCC56tT1ZN3FbjjP6Vq3YJRLN1sitaRjnsHmyhHRhK6PHsb


### 3. Transfer AVAX from X Chain to C Chain in 2 operations:

3.1) Export AVAX from X Chain to C Chain for the same account:
Note amount in NAVAX units: 1 AVAX == 1000000000 NAVAX

```
POST http://localhost:3000/export_x2c_avax
Body: {
    "networkID": 5,
    "endpoint": "api.avax-test.network",
    "privateKey": "PrivateKey-2qarB8gPPWg62Cv6HTH6au4LDLMzgRNRfA2SfNivT3sm7ZZdPw",
    "amount": 1000000000,
    "memo": "Crosschain X to C export"
}
Response:

"8qqVQ8XMMRdLiMUHpQWeekeNRT1gTLZULLA2i2crRXd7L277L"

```
Tx details: https://explorer.avax-test.network/tx/8qqVQ8XMMRdLiMUHpQWeekeNRT1gTLZULLA2i2crRXd7L277L

3.2) Import AVAX to C Chain EVM address from X Chain:
EVM address 0x3f46e6d60dd0724c3ce401c8410d014dee83c734 is derived.

```
POST http://localhost:3000/import_evm_xchain_avax
Body: {
    "networkID": 5,
    "endpoint": "api.avax-test.network",
    "privateKey": "PrivateKey-2qarB8gPPWg62Cv6HTH6au4LDLMzgRNRfA2SfNivT3sm7ZZdPw",
    "toAddress": "0x3f46e6d60dd0724c3ce401c8410d014dee83c734"
}
Response:

"24B3PAv6X8t7ZxzL5LqS7qQj81Czefd7LW3Q3sP8JgVKYQMuQD"
```
Tx details: https://explorer.avax-test.network/tx/24B3PAv6X8t7ZxzL5LqS7qQj81Czefd7LW3Q3sP8JgVKYQMuQD

### 4. Transfer AVAX from C Chain to X Chain in 2 operations:

4.1) Export AVAX from C Chain to X Chain for the same account:
Note amount in NAVAX units: 1 AVAX == 1000000000 NAVAX

```
POST http://localhost:3000/export_evm_xchain_avax
Body: {
    "networkID": 5,
    "endpoint": "api.avax-test.network",
    "privateKey": "PrivateKey-2qarB8gPPWg62Cv6HTH6au4LDLMzgRNRfA2SfNivT3sm7ZZdPw",
    "amount": 100000000,
    "toAddress": "0x3f46e6d60dd0724c3ce401c8410d014dee83c734"
}
Response:

"TODO"
```


4.2) Import AVAX to X Chain from C Chain:

```
POST http://localhost:3000/import_c2x_avax
Body: {
    "networkID": 5,
    "endpoint": "api.avax-test.network",
    "privateKey": "PrivateKey-2qarB8gPPWg62Cv6HTH6au4LDLMzgRNRfA2SfNivT3sm7ZZdPw",
    "memo": "Crosschain C to X import"
}
Response:

"TODO"
```

5) Get C-Chain Address for a given Private Key:

```
POST http://localhost:3000/get_cchain_address
Body: {
    "networkID": 5,
    "endpoint": "api.avax-test.network",
    "privateKey": "PrivateKey-kTHUYhFZL2tkmgPKo2hkjfGU53SzdexZPeJbSqPeorcM31qXg"
}
Response:
[
    "C-fuji1lyptvq35l28c6a9znmhl55e6mfgzj8zv5ec9aj"
]
```

6) Get HEX EVM Address (Ethereum format) on C-Chain for a given Private Key:

```
POST http://localhost:3000/get_hex_address
Body: {
    "privateKey": "PrivateKey-kTHUYhFZL2tkmgPKo2hkjfGU53SzdexZPeJbSqPeorcM31qXg"
}
Response:
"0xdc29f734c405f3d18386cfba2e3b4ab1b498c4cc"
```

7) Get HEX EVM Private Key (Ethereum format) for a given Avax Private Key:

```
POST http://localhost:3000/get_hex_private_key
Body: {
    "privateKey": "PrivateKey-kTHUYhFZL2tkmgPKo2hkjfGU53SzdexZPeJbSqPeorcM31qXg"
}
Response:
"62aa6e4f8a445d7139b7a544a8aec189eea9a29131e828c48989a137716d1ead"
```

# kubernetes deployment config

This directory contains the helm chart, secrets, and much of the CI/CD configuration for deploying avalanche_api to kubernetes.

## Environments
- Feature branch ephemeral deployments
- Staging
- Test
- UAT
- Production

Each environment has its own specific configuration directory that contains its appropriate Helm values and sops secrets.

## CI/CD
CI/CD will be accomplished via CircleCI (Details to be added).

## Interacting with the Kubernetes environments
- Feature, staging, test, and UAT are in the same AWS account and cluster, scoped by namespace.
- Production is in its own AWS account and cluster.

Authentication is handled via AWS IAM role assumption. [See the documentation here](https://github.com/sendwyre/wyre-infrastructure/blob/master/docs/workstation-setup.md) on how to get access to the new environments.


## Sops Secrets
Sops is a way to encrypt secret values and commit them to version control. Once your AWS credentials are configured, you'll be able to encrypt and decrypt the secret files in each given environment, if you are in a group with access to do so. Encryption and decryption of each file is done with [Mozilla sops](https://github.com/mozilla/sops) and AWS KMS-based keys.

Steps to use sops for updating secrets:

1. Decrypt a given secret file. for example:

`sops --decrypt feature/apple-certificate.secret.sops.yml > feature/apple-certificate.secret.yml`

2. Update the secret.yml file.

3. Re-encrypt the secret.yml file:

` sops --encrypt feature/apple-certificate.secret.yml > feature/apple-certificate.secret.sops.yml`

4. Commit to git.

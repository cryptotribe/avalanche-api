{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "avalanche_api.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "avalanche_api.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "avalanche_api.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Output comma separated list of hostnames
*/}}
{{- define "avalanche_api.hostnames" -}}
{{- if .Values.service.includeSanitizedBranch -}}
{{- printf "%s-%s.%s" .Values.sanitizedBranch .Values.service.serviceUrlBase .Values.service.domainName -}}
{{- else -}}
{{- printf "%s.%s" .Values.service.serviceUrlBase .Values.service.domainName -}}
{{- end -}}
{{- end -}}


{{- define "avalanche_api.sanitizedPrefix" -}}
{{- if .Values.sanitizedBranch -}}
{{- printf "%s." .Values.sanitizedBranch -}}
{{- end -}}
{{- end -}}
